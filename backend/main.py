#!/usr/bin/env python3

# Jack compiler backend
# (converts VM bytecode to Hack ASM)

# call this script with a single .vm file or a directory as the argument
# in case of directory, all VM files contained in it are compiled
# use a new parser for each file, but only one code writer

# TODO allow calling with directory

import sys
from parser import Parser, CTYPE
from code_writer import CodeWriter

if len(sys.argv) < 2:
	print('Must provide name of file or directory to translate')

parser = Parser(sys.argv[1])
writer = CodeWriter(sys.argv[1][:-2]+'asm') # a.vm -> a.asm


while parser.has_more_commands():
	parser.advance()
	ctype = parser.ctype
	arg1 = parser.arg1
	arg2 = parser.arg2

	# PUSH/POP COMMAND
	if ctype == CTYPE.PUSH or ctype == CTYPE.POP:
		writer.write_push_pop(ctype, parser.arg1, parser.arg2)

	# PROGRAM FLOW COMMAND
	elif ctype == CTYPE.LABEL or ctype == CTYPE.GOTO or ctype == CTYPE.IF:
		writer.write_program_flow(ctype, parser.arg1)

	# FUNCTION CALLING COMMAND
	elif ctype == CTYPE.RETURN:
		writer.write_return()
	elif ctype == CTYPE.FUNCTION:
		writer.write_function_declaration(parser.arg1, parser.arg2)
	elif ctype == CTYPE.CALL:
		writer.write_function_call(parser.arg1, parser.arg2)

	# ARITHMETIC COMMAND
	else:
		writer.write_arithmetic(parser.arg1)

writer.close()
