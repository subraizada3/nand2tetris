# code_writer - can be used for multiple input files

# functions:
# void __init__(output_file)
# set_file_name(string file_name)
#   notifies that translation of new VM file has started
# write_arithmetic(string command)
#   writes out asm code for the given arithmetic command
# write_push_pop(COMMAND_TYPE type, string segment, int index)
#   type is either C_PUSH or C_POP
# void close()

# Segment RAM locations:
# Main stack: RAM[0] == R0 == SP points to top of stack
# Local: RAM[1] == LCL points to local
# Argument: RAM[2] == ARG points to argument
# Static: RAM 16 to 255
# Constant: pseudo-segment holds all numbers from 0-32767 (not negative numbers)
# This: RAM[3] == THIS
# That: RAM[4] == THAT
# Pointer: equivalent to RAM[3] and RAM[4]
#          set pointer[0] to change beginning location of THIS, same with ptr[1]
# Temp: RAM 5 to 12 (size 8)
# RAM 13-15 (R13, R14, R15) are free for use by VM implementation

# R13 is used by sub and by write_push_pop
# R14 is used as the return address for eq, gt, lt

# Standard mapping:
# 16-255: static variables (all VM functions in the VM program)
# 256-2047: stack
# 2048-16483: heap

# STACK POINTER POINTS TO JUST BEYOND THE TOP OF THE STACK!!!
# true = -1 = 0bFFFF
# false = 0 = 0b0000

from parser import CTYPE
from typing import TextIO

class CodeWriter:

	# private fields
	out: TextIO
	counter: int = 0 # counter to generate globally unique labels

	def __init__(self, out_filename):
		self.out = open(out_filename, 'w')
		self.write_stdlib()

	def set_file_name(self, name):
		self.out.close()
		self.out = open(name, 'w')
		self.write_stdlib()

	def write_arithmetic(self, command):
		# possible commands (* unary, ^ bitwise):
		# add   sub   neg*   eq   gt   lt   and^   or^   not*^
		# gt, lt do x > y and x < y, with y on the top of the stack and x below it
		if   command == 'add': self.do_arithmetic_add()
		elif command == 'sub': self.do_arithmetic_sub()
		elif command == 'neg': self.do_arithmetic_neg()
		elif command == 'eq' : self.do_arithmetic_eq()
		elif command == 'gt' : self.do_arithmetic_gt()
		elif command == 'lt' : self.do_arithmetic_lt()
		elif command == 'and': self.do_arithmetic_and()
		elif command == 'or' : self.do_arithmetic_or()
		elif command == 'not': self.do_arithmetic_not()


	#   ____ _____  _    _   _ ____    _    ____  ____    _     ___ ____
	#  / ___|_   _|/ \  | \ | |  _ \  / \  |  _ \|  _ \  | |   |_ _| __ )
	#  \___ \ | | / _ \ |  \| | | | |/ _ \ | |_) | | | | | |    | ||  _ \
	#   ___) || |/ ___ \| |\  | |_| / ___ \|  _ <| |_| | | |___ | || |_) |
	#  |____/ |_/_/   \_\_| \_|____/_/   \_\_| \_\____/  |_____|___|____/
	#
	# These assembly snippets are used by other methods in this file. They should
	#    always be written to the top of the asm file.
	#
	def write_stdlib(self):
		# don't execute the stdlib on program start
		self.out.write('@__VM_STDLIB_COMPLETE__\n')
		self.out.write('0;JMP\n')
		self.write_stdlib_eq()
		self.write_stdlib_gt()
		self.write_stdlib_lt()
		self.out.write('(__VM_STDLIB_COMPLETE__)\n')

	def __OBSOLETE_README_write_stdlib_eq(self):
		# This (eq) is a long function. There is only one line different
		#   between this, gt, and lt, so they have been split into a different
		#   function. write_stdlib_eq(), gt, and lt all call the common function
		#   (write_stdlib_comparison()) with the correct different line.
		# This has been retained for documentation.

		# make a label so other functions can jump here
		# see comment for do_arithmetic_eq() for why/how this is needed/works
		self.out.write('(__VM_STDLIB_EQ__)\n')

		# current stack state: BASE | a b c d (x-y) SP
		# decrement SP to point to (x-y)
		# if it is == 0, then x and y were equal, so we set it to -1 (true)
		# otherwise, we set it to 0 (false)
		# after that, we jump back to the address in R14

		self.move_sp_to_top() # now A and SP both point to (x-y)

		# save the subtraction result (x-y) into D
		self.out.write('D=M\n')

		# ZERO/NONZERO CHECK AND JUMP
		# load the address we need to jump to if it's not equal (== false)
		self.out.write('@__VM_STDLIB_EQ_FALSE__\n')
		self.out.write('D;JNE\n') # if (x-y) != 0, jump to false part
		# if it is true (top of stack is zero), then continue onto the true part

		# IT IS ZERO: SET IT TO -1 AND JUMP PAST THE NEXT PART TO THE EQ RETURN
		# if we got here, they are equal (== true), set top of stack (M) to -1
		self.out.write('@SP\n')   # make A a pointer to the stack pointer
		self.out.write('A=M\n')   # A points to top number in stack
		self.out.write('M=-1\n')  # make it -1
		# jump past the false part back to the eq call return
		self.out.write('@__VM_STDLIB_EQ_RETURN__\n')
		self.out.write('0;JMP\n')

		# IT IS NOT ZERO: SET IT TO 0
		self.out.write('(__VM_STDLIB_EQ_FALSE__)\n')
		# set top of stack (M) to 0 to indicate false
		self.out.write('@SP\n')   # make A a pointer to the stack pointer
		self.out.write('A=M\n')   # A points to top number in stack
		self.out.write('M=0\n')   # make it 0
		# continue on to the eq call return

		# EQ CALL RETURN JUMP
		# make SP point just beyond the top of the stack again
		self.out.write('(__VM_STDLIB_EQ_RETURN__)\n')
		self.increment_sp()       # make SP point just beyond end of stack again
		self.out.write('@R14\n')  # make A point to return jump address
		self.out.write('A=M\n')   # load return jump address into A
		self.out.write('0;JMP\n')

	def write_stdlib_eq(self):
		# see above function: __OBSOLETE__README_write_stdlib_eq()
		self.write_stdlib_comparison('EQ', 'D;JNE') # false if (x-y) != 0

	def write_stdlib_gt(self):
		# see write_stdlib_eq()
		self.write_stdlib_comparison('GT', 'D;JLE') # false if (x-y) <= 0

	def write_stdlib_lt(self):
		# see write_stdlib_eq()
		self.write_stdlib_comparison('LT', 'D;JGE') # false if (x-y) >= 0

	def write_stdlib_comparison(self, name: str, comparison_line: str):
		# See __OBSOLETE_README_write_stdlib_eq() for documentation. This only
		#   contains minimal comments.
		# Name is one of the strings EQ, GT, or LT
		# comparison_line is a line of asm which should inspect the D register,
		#   which contains the value (x-y), and jump if the condition is false.
		# Don't include the newline for it. Examples:
		# eq: D;JNE   - x!=y if (x-y) is not equal to zero, so JNE
		# gt: D;JLE   - x<=y if (x-y) is less than or equal to zero, so JLE
		# lt: D;JGE   - x>=y if (x-y) is more than or equal to zero, so JGE

		# function label declaration, so other asm can call this function
		self.out.write('(__VM_STDLIB_' + name + '__)\n')
		self.move_sp_to_top()   # points to top number in stack ((x-y) here)
		self.out.write('D=M\n') # save x-y into D
		# if comparison is false, jump to the false part (which writes 0 onto stack)
		self.out.write('@__VM_STDLIB_' + name + '_FALSE__\n')
		self.out.write(comparison_line + '\n')
		# if we got here, comparison is true, overwrite top of stack with -1
		self.out.write('@SP\n')
		self.out.write('A=M\n')
		self.out.write('M=-1\n')
		# skip the part where we put 0 (false) onto the stack; return to caller
		self.out.write('@__VM_STDLIB_' + name + '_RETURN__\n')
		self.out.write('0;JMP\n')
		# comparison is false, overwrite top of stack with 0
		self.out.write('(__VM_STDLIB_' + name + '_FALSE__)\n')
		self.out.write('@SP\n')
		self.out.write('A=M\n')
		self.out.write('M=0\n')
		# jump to return to caller (ROM location stored in R14)
		self.out.write('(__VM_STDLIB_' + name + '_RETURN__)\n')
		self.increment_sp() # make SP point just beyond end of stack again
		self.out.write('@R14\n')
		self.out.write('A=M\n')
		self.out.write('0;JMP\n')



	#   _   _ _____ _     ____  _____ ____  ____
	#  | | | | ____| |   |  _ \| ____|  _ \/ ___|
	#  | |_| |  _| | |   | |_) |  _| | |_) \___ \
	#  |  _  | |___| |___|  __/| |___|  _ < ___) |
	#  |_| |_|_____|_____|_|   |_____|_| \_\____/
	#
	def increment_sp(self):
		# see move_sp_to_top() - that method decrements SP
		self.out.write('@SP   \n')
		self.out.write('M=M+1 \n')

	def move_sp_to_top(self):
		# decrements SP to point to top number in stack,
		#   instead of beyond top of stack
		# leaves A pointing to top of stack (so M == top number in stack)
		self.out.write('@SP   \n')   # load pointer to stack pointer (== 0) into A
		self.out.write('M=M-1 \n')   # decrement stack pointer
		self.out.write('A=M   \n')   # make A point to top of stack



	#      _    ____  ___ _____ _   _ __  __ _____ _____ ___ ____
	#     / \  |  _ \|_ _|_   _| | | |  \/  | ____|_   _|_ _/ ___|
	#    / _ \ | |_) || |  | | | |_| | |\/| |  _|   | |  | | |
	#   / ___ \|  _ < | |  | | |  _  | |  | | |___  | |  | | |___
	#  /_/   \_\_| \_\___| |_| |_| |_|_|  |_|_____| |_| |___\____|
	#
	# ADD SUB NEG NOT
	def do_arithmetic_add(self):
		self.move_sp_to_top()        # A now points to top number in the stack
		self.out.write('D=M   \n')   # load top number of stack into D
		self.out.write('A=A-1 \n')   # decrement A (which is being used as SP)
		self.out.write('M=D+M \n')   # add second number in stack to D
		# actual stack pointer is still pointing just beyond the end of the stack
		#   because we decremented A to get the second number, not the actual SP

	def do_arithmetic_sub(self):
		# see do_arithmetic_add() - this just subtracts instead of adding
		# this is a bit tricky though
		# stack: BASE | a b c d x y
		# we need to do x-y, not y-x
		self.move_sp_to_top()
		self.out.write('D=M    \n')  # load Y into D
		self.out.write('@R13   \n')  # load pointer to R13 into A
		self.out.write('M=D    \n')  # store Y into R13
		self.move_sp_to_top()        # decrement SP to point to X
		self.out.write('D=M    \n')  # load X into D
		self.out.write('@R13   \n')  # load pointer to R13 into A
		self.out.write('D=D-M  \n')  # D == X and R13 == Y, so this does D=X-Y
		# now, put D into where the SP is pointing
		self.out.write('@SP    \n')  # load pointer to pointer to SP into A
		self.out.write('A=M    \n')  # load pointer to SP into A
		self.out.write('M=D    \n')  # put D (== X-Y) onto top of stack
		self.increment_sp()          # make SP point beyond top of stack again

	def do_arithmetic_neg(self):
		self.move_sp_to_top()        # see do_arithmetic_add()
		self.out.write('M=-M  \n')   # top of stack = -(top of stack)
		self.increment_sp()          # make SP point beyond top of stack again

	def do_arithmetic_not(self):
		self.move_sp_to_top()        # see do_arithmetic_add()
		self.out.write('M=!M  \n')   # top of stack = !(top of stack)
		self.increment_sp()          # make SP point beyond top of stack again



	# EQ GT LT
	def do_arithmetic_eq(self):
		# you should probably read the comments and assembly in add, sub, neg, not,
		#   and, or before reading this function

		# EQ (and GT/LT)

		# If the stack is BASE | a b c d x y SP
		# Do sub, so the stack becomes | a b c d (x-y) SP
		# Then, jump to the stdlib eq, gt, or lt function. Those functions will look
		#   at the (x-y) in the stack and replace it with -1 (T) or 0 (F)
		# But we need some way to get from the stdlib at the top of the asm file
		#   back to here. So in the asm, right after the call to the stdlib eq, we
		#   make a label for the return address and store it into R14. The stdlib
		#   eq/gt/lt will jump back to the address in R14 after finishing.

		self.do_arithmetic_sub()              # do the sub

		# load the label for the return address (right after stdlib eq call) into
		#   R14, and then jump to the stdlib eq function
		self.out.write('@__VM_RETURN_EQ_' + str(self.counter) + '__\n')
		self.out.write('D=A \n') # store return address into D
		self.out.write('@R14\n') # load address of R14 into A
		self.out.write('M=D \n') # load return address from D into R14

		# return address set, call stdlib return function
		self.out.write('@__VM_STDLIB_EQ__\n') # load stdlib eq address into A
		self.out.write('0;JMP\n')             # and jump to the eq function

		# address for the stdlib eq function to return to after finishing
		self.out.write('(__VM_RETURN_EQ_' + str(self.counter) + '__)\n')
		self.counter += 1 # Important!

	def do_arithmetic_gt(self):
		# see comments in do_arithmetic_eq() for documentation on this function
		# only difference is that we call stdlib GT instead of EQ
		self.do_arithmetic_sub()
		self.out.write('@__VM_RETURN_GT_' + str(self.counter) + '__\n')
		self.out.write('D=A \n')
		self.out.write('@R14\n')
		self.out.write('M=D \n')
		self.out.write('@__VM_STDLIB_GT__\n')
		self.out.write('0;JMP\n')
		self.out.write('(__VM_RETURN_GT_' + str(self.counter) + '__)\n')
		self.counter += 1

	def do_arithmetic_lt(self):
		# see comments in do_arithmetic_eq() for documentation on this function
		# only difference is that we call stdlib LT instead of EQ
		self.do_arithmetic_sub()
		self.out.write('@__VM_RETURN_LT_' + str(self.counter) + '__\n')
		self.out.write('D=A \n')
		self.out.write('@R14\n')
		self.out.write('M=D \n')
		self.out.write('@__VM_STDLIB_LT__\n')
		self.out.write('0;JMP\n')
		self.out.write('(__VM_RETURN_LT_' + str(self.counter) + '__)\n')
		self.counter += 1



	# AND OR
	def do_arithmetic_and(self):
		self.move_sp_to_top()        # see do_arithmetic_add()
		self.out.write('D=M   \n')   # load top number in stack into D
		self.out.write('A=A-1 \n')   # decrement A (which is being used as SP)
		self.out.write('M=D&M \n')   # & second number in stack (M) with first (D)
		# see do_arithmetic_add() for note on SP

	def do_arithmetic_or(self):
		# see do_arithmetic_and() - this just |s instead of &ing
		self.move_sp_to_top()
		self.out.write('D=M   \n')
		self.out.write('A=A-1 \n')
		self.out.write('M=D|M \n')



	#   ____  _   _ ____  _   _    ______   ___  ____
	#  |  _ \| | | / ___|| | | |  / /  _ \ / _ \|  _ \
	#  | |_) | | | \___ \| |_| | / /| |_) | | | | |_) |
	#  |  __/| |_| |___) |  _  |/ / |  __/| |_| |  __/
	#  |_|    \___/|____/|_| |_/_/  |_|    \___/|_|
	#
	def write_push_pop(self, command_type, segment: str, index: int):
		# PUSH: get segment[index] and push it onto the main stack
		#  POP: pop the main stack and put it into segment[index]
		# currently only 'push constant X' is implemented

		# handle 'constant' segment
		if segment == 'constant':
			if command_type == CTYPE.PUSH:
				self.out.write('@' + str(index) + '\n') # load constant to push into A
				self.out.write('D=A   \n') # store constant to push into D
				self.out.write('@SP   \n') # load pointer to stack pointer (== 0) into A
				self.out.write('A=M   \n') # load stack pointer into A
				self.out.write('M=D   \n') # load constant into top of stack
				self.increment_sp()        # make SP point beyond top of stack again
				return # Important!
			else:
				print("Attempt to do something other than PUSH the constant segment")
				sys.exit(0)

		# Procedure: Load RAM address of segment[index] into R13. This will be done
		#   differently for each segment. Then, depending on whether the operation
		#   is push or pop, we access the top of the stack and copy to/from the
		#   address R13 points to. That part is the same regardless of the segment
		#   being pushed from or popped to.

		# Load RAM address of segment[index] into R13
		# Convenience function
		def loadNumberIntoR13(number: int): # number is the RAM address to load into R13
			self.out.write('@' + str(number) + '\n') # load RAM address into A
			self.out.write('D=A \n') # save RAM address into D
			self.out.write('@R13\n') # load pointer to R13 into A
			self.out.write('M=D \n') # load RAM address from D into R13

		# some simple cases are handled here without a separate function
		if segment == 'static':
			# static occupies RAM locs 16-255
			loadNumberIntoR13(16 + index)
		elif segment == 'pointer':
			# pointer[0] is 3, pointer[1] is 4
			loadNumberIntoR13(3 + index)
		elif segment == 'temp':
			# temp is RAM[5] to RAM[12] (size == 8)
			loadNumberIntoR13(5 + index)
		else:
			if segment == 'argument': segstring = 'ARG'
			elif segment == 'local' : segstring = 'LCL'
			elif segment == 'this'  : segstring = 'THIS'
			elif segment == 'that'  : segstring = 'THAT'
			# now, load address of segment[index] into R13
			self.out.write('@' + str(index) + '\n') # load index into A
			self.out.write('D=A  \n') # put index into D
			# put pointer to pointer to segment[0] into A
			self.out.write('@' + segstring + '\n')
			self.out.write('D=D+M\n') # put (address of segment[0]) + index into D
			self.out.write('@R13 \n') # store address of segment[index] into R13
			self.out.write('M=D  \n')

		# push/pop
		if command_type == CTYPE.PUSH:
			# push: get segment[index] and put it into the stack
			self.out.write('@R13\n') # load pointer to R13 into A
			self.out.write('A=M \n') # load R13 contents (addr of seg[index]) into A
			self.out.write('D=M \n') # load contents of segment[index] into D
			self.out.write('@SP \n') # load pointer to stack pointer into A
			self.out.write('A=M \n') # load stack pointer into A
			self.out.write('M=D \n') # copy segment[index] from D into top of stack
			self.increment_sp()      # make SP point beyond top of stack again
		elif command_type == CTYPE.POP:
			# pop: get top number in stack and put it into segment[index]
			self.move_sp_to_top()    # make SP and A point to top of stack
			self.out.write('D=M \n') # load top number of stack into D
			self.out.write('@R13\n') # load pointer to R13 (addr of seg[index]) into A
			self.out.write('A=M \n') # load address of segment[index] into A
			self.out.write('M=D \n') # copy top number of stack into segment[index]
			# SP now points beyond top of stack
		else:
			print('Error in write_push_pop')
			sys.exit(0)

	def close(self):
		# it's OK to close a file twice (see set_file_name())
		self.out.close()



	#   ____  ____   ___   ____ ____      _    __  __   _____ _     _____        __
	#  |  _ \|  _ \ / _ \ / ___|  _ \    / \  |  \/  | |  ___| |   / _ \ \      / /
	#  | |_) | |_) | | | | |  _| |_) |  / _ \ | |\/| | | |_  | |  | | | \ \ /\ / /
	#  |  __/|  _ <| |_| | |_| |  _ <  / ___ \| |  | | |  _| | |__| |_| |\ V  V /
	#  |_|   |_| \_\\___/ \____|_| \_\/_/   \_\_|  |_| |_|   |_____\___/  \_/\_/
	#
	def write_program_flow(self, command_type, label_name: str):
		if (command_type == CTYPE.LABEL):
			self.out.write('(' + label_name + ')\n')
		elif (command_type == CTYPE.GOTO):
			self.out.write('@' + label_name + '\n')
			self.out.write('0;JMP\n')
		elif (command_type == CTYPE.IF): # if-goto: jump if top of stack is not zero
			self.move_sp_to_top()   # make SP and A point to top of stack
			self.out.write('D=M\n') # load number at top of stack into D
			self.out.write('@' + label_name + '\n') # load jump address into A
			self.out.write('D;JNE\n')               # and jump if D == 0



	#   _____ _   _ _   _  ____ _____ ___ ___  _   _
	#  |  ___| | | | \ | |/ ___|_   _|_ _/ _ \| \ | |
	#  | |_  | | | |  \| | |     | |  | | | | |  \| |
	#  |  _| | |_| | |\  | |___  | |  | | |_| | |\  |
	#  |_|    \___/|_| \_|\____| |_| |___\___/|_| \_|
	#
	#    ____    _    _     _     ___ _   _  ____
	#   / ___|  / \  | |   | |   |_ _| \ | |/ ___|
	#  | |     / _ \ | |   | |    | ||  \| | |  _
	#  | |___ / ___ \| |___| |___ | || |\  | |_| |
	#   \____/_/   \_\_____|_____|___|_| \_|\____|
	#
	def write_return(self):
		pass # TODO

	def write_function_declaration(self, name: str, num_local_vars: int):
		pass # TODO

	def write_function_call(self, name: str, num_args: str):
		pass # TODO
