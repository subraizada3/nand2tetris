# parser - operates on single VM file

# Functions:
# void __init__(input_file)
# boolean has_more_commands()
# void advance()
#   reads next command from input & make it the current command
#   should only be called if has_more_commands(); current command is initially empty

# Fields:
# COMMAND_TYPE (enum) command_type
#   command type of current command
#   C_ARITHMETIC, C_PUSH, C_POP, C_LABEL, C_GOTO, C_IF, C_FUNCTION, C_RETURN, C_CALL
# string arg1
#   first argument of current command
#   returns "add", "sub", etc. if type is C_ARITHMETIC
#   should not be called if command type is C_RETURN
# int arg2
#   returns second argument of current command
#   should only be called for C_PUSH, C_POP, C_FUNCTION, C_CALL

from enum import Enum
from typing import TextIO, List


class CTYPE(Enum):
	ARITHMETIC = 1
	PUSH = 2
	POP = 3
	LABEL = 4
	GOTO = 5
	IF = 6
	FUNCTION = 7
	RETURN = 8
	CALL = 9


class Parser:
	# public fields
	ctype: CTYPE
	arg1: str
	arg2: int

	# private fields
	infile: TextIO
	file: List[str]

	def __init__(self, input_file):
		# read input file into list of lines
		fileObj = open(input_file, 'r')
		self.file = fileObj.read().splitlines()
		fileObj.close()

		# remove comments
		for i in range(len(self.file)):
			if '//' in self.file[i]:
				self.file[i] = self.file[i].split('//')[0]

		# remove whitespace
		#for i in range(len(self.file)):
		#	self.file[i] = self.file[i].replace(' ', '').replace('\t', '')
		# unlike assembler, we only remove whitespace before/after the line, instead
		#   of also between words
		self.file[i] = self.file[i].strip()

		# remove empty lines
		self.file = list(filter(None, self.file))

		self.lineNum = -1
		self.line = ''

	def has_more_commands(self):
		return self.lineNum < len(self.file) - 1

	def advance(self):
		self.lineNum += 1
		self.line = self.file[self.lineNum]

		words = self.line.split()

		# MEMORY ACCESS COMMANDS
		# 'push segment index' - push segment[index] onto the main stack
		if words[0] == 'push':
			self.ctype = CTYPE.PUSH
			self.arg1 = words[1]
			self.arg2 = int(words[2])
		# 'pop segment index' - pop from main stack into segment[index]
		elif words[0] == 'pop':
			self.ctype = CTYPE.POP
			self.arg1 = words[1]
			self.arg2 = int(words[2])

		# PROGRAM FLOW COMMANDS
		# label declaration: 'label name'
		elif words[0] == 'label':
			self.ctype = CTYPE.LABEL
			self.arg1 = words[1]
		# unconditional branch: 'goto label'
		elif words[0] == 'goto':
			self.ctype = CTYPE.GOTO
			self.arg1 = words[1]
		# conditional branch: 'if-goto label'
		elif words[0] == 'if-goto':
			self.ctype = CTYPE.IF
			self.arg1 = words[1]

		# FUNCTION CALLING COMMANDS
		# function declaration: 'function name num_local_vars'
		elif words[0] == 'function':
			self.ctype = CTYPE.FUNCTION
			self.arg1 = words[1]
			self.arg2 = int(words[2])
		# function call: 'call function_name num_arguments'
		elif words[0] == 'call':
			self.ctype = CTYPE.CALL
			self.arg1 = words[1]
			self.arg2 = int(words[2])
		# return
		elif words[0] == 'return':
			self.ctype = CTYPE.RETURN

		# ARITHMETIC/LOGIC COMMANDS
		# Command type is always C_ARITHMETIC
		# Command name becomes arg1
		# 'neg' 'not' are unary
		# 'and' 'or' 'not' are bitwise
		elif words[0] in ['add', 'sub', 'neg', 'eq', 'gt', 'lt', 'and', 'or', 'not']:
			self.ctype = CTYPE.ARITHMETIC
			self.arg1 = words[0]
