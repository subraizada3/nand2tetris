# Nand to Tetris assembler & compiler
An assembler for the Hack CPU created as part of the [From Nand to Tetris](http://www.nand2tetris.org/) course from the book 'The Elements of Computing Systems.'

Created on December 10 and 11, 2017.

Now includes the Jack language implementation (September 2018).


Directories:

`assembler`: Hack assembly to Hack machine code

`backend`: Jack bytecode to Hack assembly

`frontend`: Jack code to Jack bytecode
