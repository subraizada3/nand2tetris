# C-COMMAND -> MACHINE CODE CONVERSIONS

# C-commands have asm of 1xx cccccc ddd jjj
# xx are ignored
# cccccc is 6-bit ALU input for what computation to perform
# ddd is 3-bit destination - where does ALU output go? order is ddd=ADM
# jjj is 3-bit jump specification. corresponds to jump if <, ==, and/or > than 0

# This file has methods to convert an asm C-instruction (e.g. DM=D-1;JGE) to the
#   machine code.

# Methods:
# str dest(str): gives 3-size bitstring for destination (ADM)
#                input is command.split('=')[0] (everything to the left of =)
# str jump(str): gives 3-size bitstring for jump (<=>)
#                input is command.split(';')[1] (everything to the right of =)
# str comp(str): gives 6-size bitstring for computation (ALU input)
#                input everything between the '=' and ';'

def dest(str):
	# no dest
	if str == '':
		return '000'

	LSB = '0'
	SSB = '0'
	MSB = '0'

	if 'M' in str:
		LSB = '1'
	if 'D' in str:
		SSB = '1'
	if 'A' in str:
		MSB = '1'

	return MSB + SSB + LSB


def jump(str):
	if str == '':
		return '000'
	if str == 'JEQ':
		return '010'
	if str == 'JNE':
		return '101'
	if str == 'JMP':
		return '111'

	# parsing for JGT JGE JLE JLT now
	l = '0'
	g = '0'
	e = '0'

	if str[1] == 'G': # JGT JGE
		g = '1'
	if str[1] == 'L': # JLT JLE
		l = '1'
	if str[2] == 'E': # JGE JLE
		e = '1'

	return l + e + g


def comp(str):
	# first, constants
	# second, operators on D
	# third, operators that switch on A or M
	# a=0 except when operating on M, then a=1

	# constants
	if str == '0':
		return '0101010'
	if str == '1':
		return '0111111'
	if str == '-1':
		return '0111010'

	# D operators
	if str == 'D':
		return '0001100'
	if str == '!D':
		return '0001101'
	if str == '-D':
		return '0001111'
	if str == 'D+1':
		return '0011111'
	if str == 'D-1':
		return '0001110'

	# A/M operators
	aBit = '0'
	rest = ''

	if 'M' in str:
		aBit = '1'

	if len(str) == 1:			# A
		rest = '110000'
	elif len(str) == 2:
		if '-' in str:			# -A
			rest = '110011'
		rest = '110001'			# !A
	elif str[1:] == '+1':		# A+1
		rest = '110111'
	elif str[1:] == '-1':		# A-1
		rest = '110010'
	elif str[1:] == '-D':		# A-D
		rest = '000111'
	elif str[1] == '+':			# D+A
		rest = '000010'
	elif str[1] == '-':			# D-A
		rest = '010011'
	elif str[1] == '&':			# D&A
		rest = '000000'
	else:
		rest = '010101'			# D|A

	return aBit + rest
