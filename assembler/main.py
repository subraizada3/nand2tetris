# ASSEMBLER MAIN

import code, sys
from parser import Parser
from symbolTable import SymbolTable


USE_CMD_ARGS = True # get filename from command line or from next line?
FILE_STRING = 'progs/Pong'
LETTERS = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
           'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
           'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
           'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

# get filename to assemble
fileShortName = sys.argv[2].split('.')[0] if USE_CMD_ARGS else FILE_STRING

# initialize output file and parser class and symbol table
open(fileShortName + '.hack', 'w').close() # make or clear output file
output = open(fileShortName + '.hack', 'w')

parser = Parser(fileShortName + '.asm')
symbols = SymbolTable()
# ready to assemble the code now



# COMMAND TYPES
# 'A': load someting into A register
# asm: @x, where x is a symbol or number (number should fit in 15 bits signed)
# 'L': label in code (used for GOTO)
# asm: (LOOP), (END)
# 'C': computation - do someting, place the results in A/D/M, maybe jump
# asm: ADM=COMPUTATION;JMP
#      e.g. A=D+M;JGT, DM=!D;JNE, A=0

# A single symbol table is used for both labels and symbols



# ASSEMBLER PASS 1
# Find labels in code, and add their ROM address to the symbol table

count = 0 # line / ROM instruction number - labels don't count for this!
while parser.hasMoreComands():
	parser.advance()

	# not a label command - increment the ROM instruction number
	if parser.commandType() != 'L':
		count += 1

	# label command - this doesn't count as a ROM instruction, place the
	#   instruction number it points to into the symbol table
	elif parser.commandType() == 'L':
		symbol = parser.symbol()
		# why does this iterate over the set of letters???
		# what will happen if the user gives the label (123)? complete breakage?
		for i in LETTERS:
			if i in symbol:
				symbols.addEntry(symbol, count)
				break



# ASSEMBLER PASS 2
# Replace label names in L commands with their addresses, e.g. (LOOP) -> (293)
#   Why does this need to be done???
# Replace labels and symbols in A commands
#   Labels: in the sequence   @LOOP   0;JMP   we replace LOOP with its address
#   Symbols: these are variables the programmer can use, e.g. @i, @sum, @count
#     The first time we encounter a symbol, we put it into the symbol table
#     It is assigned a memory address, starting from 16 (SymbolTable.ramAddr)
#     On later occurences, we replace it with its assigned RAM address
parser.reset()
while parser.hasMoreComands():
	parser.advance()

	if parser.commandType() == 'L':
		parser.changeLine(parser.getLine().replace(parser.symbol(), str(symbols.getAddress(parser.symbol()))))

	if parser.commandType() == 'A':
		symbol = parser.symbol()
		for i in LETTERS:
			if i in symbol: # if it doesn't have a letter, it's e.g. @123 so skip it
				if symbols.contains(symbol): # replace symbol with its value
					parser.changeLine(parser.getLine().replace(symbol, str(symbols.getAddress(symbol))))
				else: # give it a RAM address
					symbols.addEntry(symbol, symbols.ramAddr)
					symbols.ramAddr += 1
					parser.changeLine(parser.getLine().replace(symbol, str(symbols.getAddress(symbol))))
				break



# ASSEMBLER PASS 3
# Convert commands to machine code
# A commands become 0XXX_XXXX_XXXX_XXXX, where X is the 15-bit number to load
#   into the A register.
# C commands are turned into the their 16-bit representation.
#   They begin with 1, next 2 bits are ignored (we leave them as 11).
# L commands are ignored; they do not go into the output.
parser.reset()
while parser.hasMoreComands():
	parser.advance()
	commandType = parser.commandType()
	if commandType == 'A':
		output.write('0' + format(int(parser.symbol()), '#017b')[2:] + '\n')
	elif commandType == 'C':
		output.write('111' + code.comp(parser.comp()) + code.dest(parser.dest()) + code.jump(parser.jump()) + '\n')

# Close the output file
output.close()
