# PARSER

# Consumes input asm and tokenizes it and stuff

# Methods:

# void __init__(filename): opens file and prepares it for processing
# void reset(): reset parser back to the beginning of the file
# bool hasMoreCommands()
# void advance(): move to the next line of code in the asm file

# All below methods provide information about the current line being examined:
# str  comandType(): 'A', 'L', or 'C'
# str  symbol(): the symbol used in A or L commands
#                (LOOP) -> LOOP, @sum -> sum, @123 -> 123
# str  dest(): for C-instructions, the destination field (ADM); '' if none
# str  jump(): for C-instructions, the jump field (e.g. JEQ); '' if none
# str  comp(): for C-instructions, the computation field (e.g. A+D)
# str  getLine(): get the string of the current line being processed
#                 parser removes all comments and whitespace
# void changeLine(str): replace the current line with the given string
#                       replacement should not contain any whitespace

class Parser:
	def __init__(self, file):
		# read input file into list of lines
		fileObj = open(file, 'r')
		self.file = fileObj.read().splitlines()
		fileObj.close()

		# remove comments
		for i in range(len(self.file)):
			if '//' in self.file[i]:
				self.file[i] = self.file[i].split('//')[0]

		# remove whitespace
		for i in range(len(self.file)):
			self.file[i] = self.file[i].replace(' ', '').replace('\t', '')

		# remove empty lines
		self.file = list(filter(None, self.file))

		self.lineNum = -1
		self.line = ''

	def reset(self):
		self.lineNum = -1
		self.line = ''

	def hasMoreComands(self):
		return self.lineNum < len(self.file) - 1

	def advance(self):
		self.lineNum += 1
		self.line = self.file[self.lineNum]

	def commandType(self):
		if self.line[0] == '@':
			return 'A'
		if self.line[0] == '(':
			return 'L'
		return 'C'

	def symbol(self):
		return self.line.replace('@', '').replace('(', '').replace(')', '')

	def dest(self):
		if '=' in self.line:
			return self.line.split('=')[0]
		# no dest
		return ''

	def comp(self):
		# 3 possibilities: X=X;X  X=X  X;X
		if '=' in self.line:
			postEqual = self.line.split('=')[1]

			# 1: X=X;X
			if ';' in postEqual:
				return postEqual.split(';')[0]

			# 2: X=X
			return postEqual

		# 3: X;X
		return self.line.split(';')[0]

	def jump(self):
		if ';' in self.line:
			return self.line.split(';')[1]
		# no jump
		return ''

	def getLine(self):
		return self.line

	def changeLine(self, newLine):
		self.file[self.lineNum] = newLine
		self.line = newLine
