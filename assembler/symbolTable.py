# SYMBOL TABLE

# Maintains a dictionary of label name -> memory address mapping
# Default symbols (SP, LCL, etc.) are set in constructor

# Used for both labels in code (used to point to a ROM instruction & for GOTOs),
#   and for symbols used by the asm programmer (e.g. x, height, i).
# They're not stored in a different data structure, so aside from the convention
#   of labels being all caps (LOOP, END) and symbols not being all caps (x, sum)
#   there's nothing preventing conflicts.

# For labels, we just do symbols[label_name] = ROM address
# For symbols, we need to allocate RAM addresses to put them in. We do this
#   starting at 16 and going up from there, e.g. if the programmer uses the
#   symbols 'i', 'sum', and 'iter' in his program, they will go in addresses
#   16, 17, and 18. The 'ramAddr' field in this class stores the address where
#   the next symbol should be stored, it should be incremented after inserting
#   a symbol into the table.
#   E.g.  symbols[symbol_name] = symbols.ramAddr;   ++symbols.ramAddr

# Methods:
# void addEntry(str symbol_name, int address)
# bool contains(str symbol_name)
#  int getAddress(str symbol_Name)

class SymbolTable:
	def __init__(self):
		self.symbols = {
			'SP': 0,
			'LCL': 1,
			'ARG': 2,
			'THIS': 3,
			'THAT': 4,
			'SCREEN': 16384,
			'KBD': 24576
		}

		for i in range(16):
			self.symbols['R' + str(i)] = i

		self.ramAddr = 16


	def addEntry(self, symbol: str, address: int):
		self.symbols[symbol] = address

	def contains(self, symbol: str) -> bool:
		return symbol in self.symbols

	def getAddress(self, symbol: str) -> int:
		return self.symbols[symbol]
